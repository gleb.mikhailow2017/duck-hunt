﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    public float maxt, mint;

    public bool startG;

    public float spawnT;
    [SerializeField] float tmp;
    public GameObject gg;
    void Start()
    {
        tmp = spawnT;
        startG = false;
    }

    public void startt(bool start,int min, int max)
    {
        mint = min;
        maxt = max;
        startG = start;

    }
    void Update()
    {
        if (startG)
        {
            if (tmp <= 0)
            {
                spawnT = Random.Range(mint, maxt);
                tmp = spawnT;
                GameObject b = GameObject.Instantiate(gg, gameObject.transform);
                b.transform.localPosition = new Vector3(0,0,0);
                b.transform.SetParent(transform.parent);
            }
            tmp -= Time.deltaTime;
        }
    }
}
