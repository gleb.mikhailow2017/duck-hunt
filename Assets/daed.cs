﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class daed : MonoBehaviour, IBeginDragHandler
{
    GameManager gm;
    private void Start()
    {
        gm = GameObject.FindObjectOfType<GameManager>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        gm.lifes--;
        Destroy(gameObject);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        gm.score++;
        Destroy(gameObject);
    }
}
