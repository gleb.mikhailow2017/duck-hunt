﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int score = 0, maxscore, lifes; 

    public GameObject sp1, sp2, sp3, sp4;
    public float time;
    bool stop;
    float i;

    public void StartT()
    {
        sp1.GetComponent<spawner>().startt(true, 5, 8);
        sp2.GetComponent<spawner>().startt(true, 5, 8);
        sp3.GetComponent<spawner>().startt(true, 5, 8);
        sp4.GetComponent<spawner>().startt(true, 5, 8);
        stop = false;
        lifes = 3;
    }

    private void Update()
    {

        if (stop)
        {
            i = time;
            sp1.GetComponent<spawner>().startt(false, 5, 8);
            sp2.GetComponent<spawner>().startt(false, 5, 8);
            sp3.GetComponent<spawner>().startt(false, 5, 8);
            sp4.GetComponent<spawner>().startt(false, 5, 8);
        }
        if (!stop)
            i -= Time.deltaTime;
        if (i <= 0)
            stop = true;
        if (lifes<=0)
        {
            maxscore = score;
            score = 0;
            lifes = 3;
            stop = true;
        }
    }

}
